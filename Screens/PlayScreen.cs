﻿using GameEngine;
using GameEngine.GUI;
using GameEngine.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using GameEngine.Logger;
using GameEngine.Utilities;
using Math = System.Math;

namespace Do_not_get_angry_man.Screens
{
    class PlayScreen : GameScreen
    {
        enum GameState
        {
            Setup,
            Run,
            GameOver
        }

        enum TurnState
        {
            Undevined,
            RollDice,
            RollDiceAnimation,
            MovementOfThePawn,
            EndTurn
        }
        private GameState gameState = GameState.Setup;
        private TurnState turnState = TurnState.Undevined;


        private Texture2D cellImage;
        private Texture2D pawnImage;
        private Texture2D hatImage;
        private Texture2D ballonImage;

        private Texture2D diceImage;
        private Texture2D hourglassImage;

        private Texture2D backgroundImage;

        private SpriteFont fontSmall;
        private SpriteFont fontNormal;
        private SpriteFont fontLarge;
        private SpriteBatch spriteBatch;

        private GameBoard board;

        private GameManager manager;

        private InputHandler input;

        private GUI gui;

        private ImageButton rollButton;
        private ImageButton endButton;
        private Vector2 screenSize;

        private Rectangle okButtonBounds;

        private Rectangle backButtonRectangle = new Rectangle(300, 600, 600, 100);

        private int playerTurn;

        //GUI ELEMENTS
        SimpleImagesAtlas dices;


        private Button BackToMenuButton;
        private Button AgainButon;


        Dice die;

        List<Player> players;


        private bool waitForNextPlayer = false;
        private Button NextPlayerButton;


        Player winingPlayer = null;

        private int dice;
        int currentPlayer = -1;

        public PlayScreen(int playersCount, bool sort)
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5f);
            TransitionOffTime = TimeSpan.FromSeconds(0.5f);
            manager = new GameManager();
            this.board = new GameBoard();

            HumanPlayerController humanPlayerController = new HumanPlayerController();
            humanPlayerController.OnPawnSelect += PawnSelect;

            AIPlayerController aIPlayerController = new AIPlayerController(new EasyAIStrategy());
            aIPlayerController.OnPawnSelect += PawnSelect;

            players = new List<Player>();
            //for (int i = 1; i <= playersCount; ++i)
            //{
            //    players.Add(new Player("Player" + i, players.Count + PlayerIndex.One, humanPlayerController));

            for(int i = 1; i <= playersCount; ++i)
            {
                players.Add(new Player("AI Player " + i, players.Count + PlayerIndex.One, aIPlayerController));
            }

            //players.Add(new Player("AI Player" + 2, players.Count + PlayerIndex.One, aIPlayerController));
            //players.Add(new Player("AI Player" + 3, players.Count + PlayerIndex.One, aIPlayerController));
            //players.Add(new Player("AI Player" + 4, players.Count + PlayerIndex.One, aIPlayerController));
            //players.Add(new Player("AI Player" + 5, players.Count + PlayerIndex.One, aIPlayerController));
            //players.Add(new Player("AI Player" + 6, players.Count + PlayerIndex.One, aIPlayerController));


            //}


            this.board.sort = sort;
            this.board.OnPawnSelect += PawnSelectOnTheBoard;
            CheckDate();
        }

        public PlayScreen(List<PlayerInfo> players)
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5f);
            TransitionOffTime = TimeSpan.FromSeconds(0.5f);
            this.manager = new GameManager();
            this.board = new GameBoard();

            this.players = new List<Player>();

            foreach(PlayerInfo player in players)
            {
                this.players.Add(new Player(player.Nickname, PlayerIndex.One + this.players.Count, player.Controller));
            }

            this.board.sort = true;
            this.board.OnPawnSelect += PawnSelectOnTheBoard;

            Logger.LogMessage($"Start game for {this.players.Count} players");
            foreach(Player player in this.players)
            {
                player.Controller.OnPawnSelect += PawnSelect;

                Logger.LogMessage($"{player.ToString()} is {(player.Controller is HumanPlayerController ? "Human" : "AI " + ((AIPlayerController)player.Controller).SelectedStrategy.ToString())}");
            }
            CheckDate();
        }

        private void CheckDate()
        {
            var today = DateTime.Now;

            this.board.drawBallon = (today.Day == 14 && today.Month == 2); // Valentine's Day
            this.board.drawHat = (today.Month == 12 && (today.Day == 06 || (today.Day >= 24 && today.Day <= 26))); // Saint Nicholas Day or Christmas

        }


        private void PawnSelectOnTheBoard(Pawn pawn)
        {
            if(currentPlayer != -1)
            {
                if(players[currentPlayer].Controller is HumanPlayerController)
                {
                    HumanPlayerController controller = players[currentPlayer].Controller as HumanPlayerController;
                    controller.PawnSelect(pawn);
                }
            }
        }

        private void PawnSelect(Pawn pawn)
        {
            if(this.manager.CanSelectPawn)
            {
                if(pawn.Player.Equals(manager.CurentPlayerTurn))
                {
                    this.manager.SelectPawn(pawn);
                }
                else
                {
                    this.manager.SelectPawn(null);
                }
            }
        }

        public void RollOfDice()
        {
            if(currentPlayer != -1)
            {
                if(players[currentPlayer].Controller is HumanPlayerController)
                {
                    HumanPlayerController controller = players[currentPlayer].Controller as HumanPlayerController;
                    controller.RollOfDice();
                }
            }
        }


        public void EndTurnButtonClick()
        {
            if(currentPlayer != -1)
            {
                if(players[currentPlayer].Controller is HumanPlayerController)
                {
                    Logger.LogMessage("EndTrun Clicked by" + players[currentPlayer]);
                    HumanPlayerController controller = players[currentPlayer].Controller as HumanPlayerController;
                    controller.EndTurn();

                }
            }
        }
        public void EndTurn()
        {
            Logger.LogMessage("EndTrun" + players[currentPlayer]);

            if(players[playerTurn].Controller is HumanPlayerController)
            {
                waitForNextPlayer = true;
                NextPlayerButton.Bounds = okButtonBounds;
                NextPlayerButton.IsActive = true;
            }
            else
            {
                playerTurn = (playerTurn + 1) % players.Count;
            }

            endButton.Position = screenSize;
            endButton.IsActive = false;
        }

        public void NextPlayer()
        {
            NextPlayerButton.Bounds = new Rectangle(screenSize.ToPoint(), new Vector2(100, 50).ToPoint());
            NextPlayerButton.IsActive = false;
            waitForNextPlayer = false;
            playerTurn = (playerTurn + 1) % players.Count;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            gui.Update(gameTime);


            // HumanPlayerController Input
            //DoTo: Refactor to key map or etc.


            switch(gameState)
            {
                case GameState.Run:
                this.board.Update(input, gameTime);
                foreach(Player player in players)
                {
                    if(player.SecurePawnsCount == 4)
                    {
                        gameState = GameState.GameOver;
                        winingPlayer = player;
                        Logger.LogMessage("Game is Over, win " + winingPlayer.Nickname);
                        rollButton.Position = screenSize;
                        rollButton.IsActive = false;
                        endButton.Position = screenSize;
                        endButton.IsActive = false;
                    }
                }


                if(!waitForNextPlayer)
                {

                    switch(turnState)
                    {
                        case TurnState.RollDice:

                        if(manager.CanSelectPawn)
                        {
                            manager.CanSelectPawn = false;
                        }
                        if(manager.CanRoolOfTheDice())
                        {
                            if(players[currentPlayer].Controller is HumanPlayerController)
                            {
                                rollButton.Position = screenSize - new Vector2(200);
                                rollButton.IsActive = true;
                            }


                            if(players[currentPlayer].Controller.IsRollOfDice())
                            {
                                manager.RoolOfTheDice();
                                if(players[currentPlayer].Controller is HumanPlayerController)
                                {
                                    rollButton.Position = screenSize;
                                    rollButton.IsActive = false;
                                }

                                turnState = TurnState.RollDiceAnimation;

                            }
                        }
                        else
                        {
                            turnState = TurnState.MovementOfThePawn;
                        }

                        break;


                        case TurnState.RollDiceAnimation:

                        if(die.AnimationEnd)
                        {
                            turnState = TurnState.MovementOfThePawn;
                        }
                        break;

                        case TurnState.MovementOfThePawn:

                        if(currentPlayer != -1 && manager.IsAvaliableMovement())
                        {
                            if(players[currentPlayer].Controller is AIPlayerController)
                            {
                                ((AIPlayerController)players[currentPlayer].Controller).Action(manager, board);
                            }
                        }

                        if(!manager.CanSelectPawn)
                        {
                            manager.CanSelectPawn = true;
                        }
                        if(manager.IsAvaliableMovement())
                        {
                            if(players[currentPlayer].activePawns > 0)
                            {
                                if(manager.PawnIsSelected)
                                {
                                    if(manager.CanMove())
                                    {
                                        manager.Move();
                                        if(!manager.CanRoolOfTheDice())
                                        {
                                            if(players[currentPlayer].Controller is HumanPlayerController)
                                            {
                                                endButton.Position = screenSize - new Vector2(200);
                                                endButton.IsActive = true;
                                            }


                                            turnState = TurnState.EndTurn;
                                            die.Stop();
                                        }
                                        else
                                        {
                                            turnState = TurnState.RollDice;
                                            die.Stop();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if(dice == 6)
                                {
                                    if(manager.PawnIsSelected)
                                    {
                                        if(manager.CanMove())
                                        {
                                            manager.Move();
                                        }

                                        if(!manager.CanRoolOfTheDice())
                                        {
                                            if(players[currentPlayer].Controller is HumanPlayerController)
                                            {
                                                endButton.Position = screenSize - new Vector2(200);
                                                endButton.IsActive = true;
                                            }

                                            turnState = TurnState.EndTurn;
                                            die.Stop();
                                        }
                                        else
                                        {
                                            turnState = TurnState.RollDice;
                                            die.Stop();
                                        }
                                    }
                                }
                                else
                                {
                                    if(!manager.CanRoolOfTheDice())
                                    {
                                        if(players[currentPlayer].Controller is HumanPlayerController)
                                        {
                                            endButton.Position = screenSize - new Vector2(200);
                                            endButton.IsActive = true;
                                        }
                                        turnState = TurnState.EndTurn;
                                        die.Stop();
                                    }
                                    else
                                    {
                                        turnState = TurnState.RollDice;
                                        die.Stop();
                                    }
                                }
                            }
                        }
                        else
                        {
                            if(!manager.CanRoolOfTheDice())
                            {
                                if(players[currentPlayer].Controller is HumanPlayerController)
                                {
                                    endButton.Position = screenSize - new Vector2(200);
                                    endButton.IsActive = true;
                                }

                                turnState = TurnState.EndTurn;
                            }
                            else
                            {
                                turnState = TurnState.RollDice;
                            }
                        }




                        break;
                        case TurnState.EndTurn:

                        if(players[currentPlayer].Controller.IsEndTurn())
                        {
                            EndTurn();
                            manager.EndTurn();
                            turnState = TurnState.RollDice;
                        }
                        break;
                    }
                }

                die.Update(gameTime);

                dice = manager.Dice;
                currentPlayer = players.FindIndex(p => p.Equals(manager.CurentPlayerTurn));

                break;

                case GameState.Setup:
                gameState = GameState.Run;
                currentPlayer = players.FindIndex(p => p.Equals(manager.CurentPlayerTurn));
                manager.DumpBoard();
                break;
                case GameState.GameOver:
                {
                    BackToMenuButton.Bounds = backButtonRectangle;
                    BackToMenuButton.IsActive = true;
                }
                break;

            }
        }

        public override void Draw(GameTime gameTime)
        {
            Vector2 screenSize = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height);

            Vector2 center = screenSize / 2;

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

            spriteBatch.Draw(backgroundImage, spriteBatch.GraphicsDevice.Viewport.Bounds, Color.White * 0.2f);
            this.board.Draw(spriteBatch, gameTime);
            switch(gameState)
            {

                case GameState.Run:

                Vector2 scale = new Vector2((float)Math.Abs(Math.Sin(gameTime.TotalGameTime.TotalSeconds * 2)) / 10.0f + 0.5f);
                Vector2 scale2 = new Vector2((float)Math.Abs(Math.Cos(gameTime.TotalGameTime.TotalSeconds * 2)) / 10.0f + 0.25f);



                Color color = Color.Lerp(Color.White, Color.Yellow, ((float)Math.Abs(Math.Sin(gameTime.TotalGameTime.TotalSeconds * 2) / 2.0) + 0.25f));

                //spriteBatch.Draw(hourglassImage, new Rectangle(new Point(1250, 780), (hourglassImage.Bounds.Size.ToVector2() * scale2).ToPoint()), null, color, 0, diceImage.Bounds.Center.ToVector2(), SpriteEffects.None, 0);
                //spriteBatch.Draw(diceImage, new Rectangle(new Point(1450, 780), (diceImage.Bounds.Size.ToVector2() * scale).ToPoint()), null, color, 0, diceImage.Bounds.Center.ToVector2(), SpriteEffects.None, 0);



                //spriteBatch.DrawString(fontSmall, "F \r\nR - Rzut kością\r\nE - koniec tury\r\nPionek wybierany przez kliknięcie nań kursorem", new Vector2(1050, 800), Color.White);


                if(dice != 0)
                {
                    spriteBatch.DrawString(fontSmall, "Wyrzucono: " + dice, Vector2.One * 10, Color.White);
                }
                if(currentPlayer != -1)
                {


                    spriteBatch.DrawString(fontSmall, "Ruch gracza : " + players[currentPlayer], new Vector2(8, 38), Color.Black);
                    spriteBatch.DrawString(fontSmall, "Ruch gracza : " + players[currentPlayer], new Vector2(12, 38), Color.Black);
                    spriteBatch.DrawString(fontSmall, "Ruch gracza : " + players[currentPlayer], new Vector2(8, 42), Color.Black);
                    spriteBatch.DrawString(fontSmall, "Ruch gracza : " + players[currentPlayer], new Vector2(12, 42), Color.Black);


                    spriteBatch.DrawString(fontSmall, "Ruch gracza : " + players[currentPlayer], new Vector2(10, 40), players[currentPlayer].Color);
                }

                spriteBatch.DrawString(fontSmall, turnState.ToString(), new Vector2(250, 10), Color.White);

                if(waitForNextPlayer)
                {
                    spriteBatch.Draw(spriteBatch.SolidTexture(), new Rectangle(Vector2.Zero.ToPoint(), screenSize.ToPoint()), Color.Black * 0.95f);
                    string message = "Ruch gracza : " + players[playerTurn].Nickname;
                    Vector2 messageSize = fontLarge.MeasureString(message);

                    spriteBatch.DrawString(fontLarge, message, center + new Vector2(-1, 1), Color.Black, 0, messageSize / 2, Vector2.One, SpriteEffects.None, 0);
                    spriteBatch.DrawString(fontLarge, message, center + new Vector2(1, 1), Color.Black, 0, messageSize / 2, Vector2.One, SpriteEffects.None, 0);
                    spriteBatch.DrawString(fontLarge, message, center + new Vector2(1, -1), Color.Black, 0, messageSize / 2, Vector2.One, SpriteEffects.None, 0);
                    spriteBatch.DrawString(fontLarge, message, center + new Vector2(-1, -1), Color.Black, 0, messageSize / 2, Vector2.One, SpriteEffects.None, 0);


                    spriteBatch.DrawString(fontLarge, message, center, players[playerTurn].Color, 0, messageSize / 2, Vector2.One, SpriteEffects.None, 0);
                }




                break;
                case GameState.GameOver:

                string endText = "      Koniec gry\r\n Wygrywa " + winingPlayer.Nickname;
                Vector2 textSize = fontLarge.MeasureString(endText);

                spriteBatch.Draw(RectangleExtension.SolidTexture(spriteBatch), spriteBatch.GraphicsDevice.Viewport.Bounds, Color.Black * 0.5f);
                spriteBatch.DrawString(fontLarge, endText, center + new Vector2(-15, -7.5f), Color.Black, 0, textSize * new Vector2(0.5f, 0.5f), new Vector2(2), SpriteEffects.None, 0);
                spriteBatch.DrawString(fontLarge, endText, center + new Vector2(-5, -2), Color.DarkGray, 0, textSize * new Vector2(0.5f, 0.5f), new Vector2(2), SpriteEffects.None, 0);
                spriteBatch.DrawString(fontLarge, endText, center, Color.White, 0, textSize * new Vector2(0.5f, 0.5f), new Vector2(2), SpriteEffects.None, 0);

                break;
            }

            gui.Draw(spriteBatch, gameTime, 1);

            string devBuild = "DEVELOPMENT BUILD <3";

            Vector2 devBuildSize = ScreenManager.DefaultFontSmall.MeasureString(devBuild);

            spriteBatch.DrawString(ScreenManager.DefaultFontSmall, devBuild, screenSize, Color.White, 0, devBuildSize, Vector2.One, SpriteEffects.None, 0);
            spriteBatch.End();
        }

        public override void Unload()
        {
            // ToDo: Remove all unmanaged resources
        }

        public override void Activate(bool instancePreserved)
        {
            if(!instancePreserved)
            {
                cellImage = ScreenManager.Content.Load<Texture2D>("circle");
                pawnImage = ScreenManager.Content.Load<Texture2D>("pawn");
                hatImage = ScreenManager.Content.Load<Texture2D>("hat");
                ballonImage = ScreenManager.Content.Load<Texture2D>("ballon");
                diceImage = ScreenManager.Content.Load<Texture2D>("dice");
                hourglassImage = ScreenManager.Content.Load<Texture2D>("hourglass");
                backgroundImage = ScreenManager.Content.Load<Texture2D>("background");

                dices = new SimpleImagesAtlas(ScreenManager.Content.Load<Texture2D>("dices"), new Point(6, 2));

                die = new Dice(1.0f);
                die.dices = dices;

                fontSmall = ScreenManager.DefaultFontSmall;
                fontNormal = ScreenManager.DefaultFontNormal;
                fontLarge = ScreenManager.DefaultFontLarge;
                this.board.cellImage = cellImage;
                this.board.pawnImage = pawnImage;
                this.board.hatImage = hatImage;
                this.board.ballonImage = ballonImage;
                spriteBatch = ScreenManager.SpriteBatch;


                screenSize = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height);
                Vector2 boardSize = screenSize * new Vector2(0.8f, 0.9f);
                Vector2 boardPosition = new Vector2(0, screenSize.Y / 2 - boardSize.Y / 2);

                this.board.Bounds = new Rectangle(boardPosition.ToPoint(), boardSize.ToPoint());
                this.board = manager.PrepareGameBoard(players, this.board);
                this.board.dice = die;
                manager.SetDice(die);

                gui = new GUI(ScreenManager.Input);

                Image img = new Image(diceImage, diceImage.Bounds);
                img.Size = new Vector2(150, 150);
                rollButton = new ImageButton(img);
                rollButton.Position = screenSize;// - new Vector2(200);
                rollButton.TextFont = ScreenManager.DefaultFontSmall;
                rollButton.BackColor = Color.White * 0.25f;
                rollButton.FrontColor = Color.White;
                rollButton.ClickAction += RollOfDice;
                rollButton.IsActive = false;
                gui.AddControl(rollButton);



                img = new Image(hourglassImage, hourglassImage.Bounds);
                img.Size = new Vector2(150, 150);
                endButton = new ImageButton(img);
                endButton.Position = screenSize;// - new Vector2(200);
                endButton.TextFont = ScreenManager.DefaultFontSmall;
                endButton.BackColor = Color.White * 0.25f;
                endButton.FrontColor = Color.White;
                endButton.ClickAction += EndTurnButtonClick;
                endButton.IsActive = false;
                gui.AddControl(endButton);


                okButtonBounds = new Rectangle((screenSize / 2 - new Vector2(50, -100)).ToPoint(), new Vector2(100, 50).ToPoint());

                NextPlayerButton = new Button("OK");
                NextPlayerButton.Bounds = new Rectangle(screenSize.ToPoint(), new Vector2(300, 50).ToPoint());
                NextPlayerButton.BackColor = Color.White * 0.25f;
                NextPlayerButton.FrontColor = Color.White * 0.75f;
                NextPlayerButton.HoverColor = Color.White;
                NextPlayerButton.TextFont = fontNormal;
                NextPlayerButton.IsActive = true;
                NextPlayerButton.ClickAction += NextPlayer;
                gui.AddControl(NextPlayerButton);



            }
            input = ScreenManager.Input;

            turnState = TurnState.RollDice;
            playerTurn = (int)manager.CurentPlayerTurn.Index;

            BackToMenuButton = new Button("menu główne");
            BackToMenuButton.Bounds = new Rectangle(screenSize.ToPoint(), new Vector2(300, 50).ToPoint());
            BackToMenuButton.BackColor = Color.White * 0.25f;
            BackToMenuButton.FrontColor = Color.White * 0.75f;
            BackToMenuButton.HoverColor = Color.White;
            BackToMenuButton.TextFont = fontNormal;
            BackToMenuButton.IsActive = false;
            BackToMenuButton.ClickAction += () =>
            {
                LoadingScreen.Load(ScreenManager, true, new MenuScreen());
            };
            gui.AddControl(BackToMenuButton);

            if(players[playerTurn].Controller is HumanPlayerController)
            {
                waitForNextPlayer = true;
                NextPlayerButton.IsActive = true;
                NextPlayerButton.Bounds = okButtonBounds;
            }
            else
            {
                playerTurn = (playerTurn + 1) % players.Count;
            }
        }


    }
}