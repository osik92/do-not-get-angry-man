using System;
using GameEngine;
using GameEngine.GUI;
using GameEngine.Screens;
using GameEngine.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Do_not_get_angry_man.Screens
{
    public class LobbyScreen : GameScreen
    {

        private Texture2D background;
        private GUI gui;
        private Label maxPlayersLabel;
        private Input playerNameInput;

        private int MaxPlayers = 2;

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                background = ScreenManager.Content.Load<Texture2D>("background");
                gui = new GUI(ScreenManager.Input);
                gui.TextFont = ScreenManager.DefaultFontLarge;


                var label = new Label("Nazwa gracza");
                label.Position = new Vector2(50, 60);
                label.FrontColor = Color.White * 0.85f;
                label.TextFont = ScreenManager.DefaultFontNormal;
                gui.AddControl(label);

                string playerName = Config.Instance.Storage.Contains("playername")
                    ? Config.Instance.Storage.Get<string>("playername")
                    : Environment.UserName;


                playerNameInput = new Input();
                playerNameInput.CanWrite = true;
                playerNameInput.Bounds = new Rectangle(300, 50, 300, 50);
                playerNameInput.BackColor = Color.White * 0.25f;
                playerNameInput.FrontColor = Color.White * 0.85f;
                playerNameInput.TextFont = ScreenManager.DefaultFontNormal;
                playerNameInput.Text = playerName;
                playerNameInput.IsActive = true;
                gui.AddControl(playerNameInput);



                label = new Label("Ilo�� graczy");
                label.Position = new Vector2(50, 160);
                label.FrontColor = Color.White * 0.85f;
                label.TextFont = ScreenManager.DefaultFontNormal;
                gui.AddControl(label);


                var button = new Button("<");
                button.Bounds = new Rectangle(300, 150, 50, 50);
                button.BackColor = Color.White * 0.25f;
                button.FrontColor = Color.White * 0.85f;
                button.HoverColor = Color.White;
                button.TextFont = ScreenManager.DefaultFontNormal;
                button.ClickAction += () => { ChangeMaxPlayersCount(false); };
                gui.AddControl(button);

                maxPlayersLabel = new Label(MaxPlayers.ToString());
                maxPlayersLabel.Position= new Vector2(450, 160);
                maxPlayersLabel.FrontColor = Color.White * 0.85f;
                maxPlayersLabel.TextFont = ScreenManager.DefaultFontNormal;
                gui.AddControl(maxPlayersLabel);


                button = new Button(">");
                button.Bounds = new Rectangle(550, 150, 50, 50);
                button.BackColor = Color.White * 0.25f;
                button.FrontColor = Color.White * 0.85f;
                button.HoverColor = Color.White;
                button.TextFont = ScreenManager.DefaultFontNormal;
                button.ClickAction += () => { ChangeMaxPlayersCount(true); };
                gui.AddControl(button);

                button = new Button("Uruchom gr�");
                button.Bounds = new Rectangle(300, 250, 300, 50);
                button.BackColor = Color.White * 0.25f;
                button.FrontColor = Color.White * 0.85f;
                button.HoverColor = Color.White;
                button.TextFont = ScreenManager.DefaultFontNormal;
                button.ClickAction += StartServer;
                gui.AddControl(button);



                var playerList = new List(6);
                playerList.TextFont = ScreenManager.DefaultFontNormal;
                playerList.Bounds = new Rectangle(650, 50, 300, 380);
                
                var listItem = new ListItem("dupa 123");
                listItem.BackColor = Color.White * 0.25f;
                listItem.FrontColor = Color.White * 0.85f;
                listItem.SelectedBackColor = Color.White * 0.5f;
                listItem.ClickAction += ClickAction;
                
                playerList.Add(listItem);


                listItem = new ListItem("dupa 321");
                listItem.BackColor = Color.Black * 0.25f;
                listItem.FrontColor = Color.White * 0.85f;
                listItem.SelectedBackColor = Color.Black * 0.5f;
                listItem.ClickAction += ClickAction;
                playerList.Add(listItem);


                listItem = new ListItem("dupa 147");
                listItem.BackColor = Color.White * 0.25f;
                listItem.FrontColor = Color.White * 0.85f;
                listItem.SelectedBackColor = Color.White * 0.5f;
                listItem.ClickAction += ClickAction;
                playerList.Add(listItem);


                listItem = new ListItem("dupa 741");
                listItem.BackColor = Color.Black * 0.25f;
                listItem.FrontColor = Color.White * 0.85f;
                listItem.SelectedBackColor = Color.Black * 0.5f;
                listItem.ClickAction += ClickAction;
                playerList.Add(listItem);


                listItem = new ListItem("dupa 852");
                listItem.BackColor = Color.White * 0.25f;
                listItem.FrontColor = Color.White * 0.85f;
                listItem.SelectedBackColor = Color.White * 0.5f;
                listItem.ClickAction += ClickAction;
                playerList.Add(listItem);


                listItem = new ListItem("dupa 369");
                listItem.BackColor = Color.Black * 0.25f;
                listItem.FrontColor = Color.White * 0.85f;
                listItem.SelectedBackColor = Color.Black * 0.5f;
                listItem.ClickAction += ClickAction;
                playerList.Add(listItem);




                gui.AddControl(playerList);
            }

        }

        private void ClickAction(ListItem sender)
        {
            playerNameInput.Text = sender.Text;

        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Vector2 screenSize = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height);
            Vector2 center = screenSize / 2;

            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(Vector2.Zero.ToPoint(), screenSize.ToPoint()), Color.White);

            spriteBatch.Draw(spriteBatch.SolidTexture(), new Rectangle(650, 50, 300, 380), Color.Black * 0.75f);

            gui.Draw(spriteBatch, gameTime, 1);
            string devBuild = "DEVELOPMENT BUILD <3";

            Vector2 devBuildSize = ScreenManager.DefaultFontSmall.MeasureString(devBuild);

            spriteBatch.DrawString(ScreenManager.DefaultFontSmall, devBuild, screenSize, Color.White, 0, devBuildSize, Vector2.One, SpriteEffects.None, 0);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (State == eScreenState.Active)
            {

                gui.Update(gameTime);
            }
        }


        private void ChangeMaxPlayersCount(bool iterate)
        {
            if (iterate)
            {
                ++MaxPlayers;
                if (MaxPlayers > 6)
                    MaxPlayers = 2;

            }
            else
            {
                --MaxPlayers;
                if (MaxPlayers < 2)
                    MaxPlayers = 6;
            }

            maxPlayersLabel.Text = MaxPlayers.ToString();
        }


        private void StartServer()
        {

            Config.Instance.Storage.Set("playername", playerNameInput.Text);

            Config.Instance.SaveStorage();
            LoadingScreen.Load(ScreenManager,true, new PlayScreen(MaxPlayers,true ) );
        }
    }
}