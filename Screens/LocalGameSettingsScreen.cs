using System;
using System.Collections.Generic;
using GameEngine.GUI;
using GameEngine.Logger;
using GameEngine.Screens;
using GameEngine.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Do_not_get_angry_man.Screens
{
    public class LocalGameSettingsScreen : GameScreen
    {

        private Texture2D background;
        private Texture2D pawnTexture;
        private GUI gui;
        private GUI playerSettingGui;


        private GUI aiPlayerControlerGui;

        private Input playerNameInput;
        private List players;

        private Button addHumanButton;
        private Button addAIButton;
        private Button startGameButton;

        private Button moveUpButton;
        private Button moveDownButton;


        private Button easyAIButton;
        private Button hardAIButton;



        private List<PlayerInfo> playerList;
        private int humanPlayers = 1;
        private int aiPlayers = 1;
        private bool playerSettingEnable = false;
        private int selectedPlayerIndex = -1;
        private AIControllers selectedAIController;

        private Random random;

        public LocalGameSettingsScreen()
        {
            playerList = new List<PlayerInfo>();
            random = new Random();
        }

        public override void Activate(bool instancePreserved)
        {
            if(!instancePreserved)
            {
                background = ScreenManager.Content.Load<Texture2D>("background");
                pawnTexture = ScreenManager.Content.Load<Texture2D>("pawn");
                gui = new GUI(ScreenManager.Input);
                gui.TextFont = ScreenManager.DefaultFontNormal;



                string playerName = Config.Instance.Storage.Contains("playername")
                    ? Config.Instance.Storage.Get<string>("playername")
                    : Environment.UserName;

                addHumanButton = new Button("Dodaj gracza");
                addHumanButton.Bounds = new Rectangle(75, 50, 225, 50);
                addHumanButton.BackColor = Color.White * 0.25f;
                addHumanButton.FrontColor = Color.White * 0.85f;
                addHumanButton.HoverColor = Color.White;
                addHumanButton.TextFont = gui.TextFont;
                addHumanButton.ClickAction += () =>
                {
                    AddPlayer(true);
                };
                gui.AddControl(addHumanButton);


                addAIButton = new Button("Dodaj SI");
                addAIButton.Bounds = new Rectangle(375, 50, 225, 50);
                addAIButton.BackColor = Color.White * 0.25f;
                addAIButton.FrontColor = Color.White * 0.85f;
                addAIButton.HoverColor = Color.White;
                addAIButton.TextFont = gui.TextFont;
                addAIButton.ClickAction += () =>
                {
                    AddPlayer(false);
                };
                gui.AddControl(addAIButton);

                players = new List(6);
                players.Bounds = new Rectangle(150, 150, 450, 450);
                players.TextFont = gui.TextFont;
                gui.AddControl(players);




                startGameButton = new Button("ROZPOCZNIJ GR�");
                startGameButton.Bounds = new Rectangle(50, 650, 700, 50);
                startGameButton.BackColor = Color.Black * 0.25f;
                startGameButton.FrontColor = Color.White * 0.85f;
                startGameButton.HoverColor = Color.White;
                startGameButton.TextFont = ScreenManager.DefaultFontLarge;
                startGameButton.IsActive = false;
                startGameButton.ClickAction += StartGame;
                gui.AddControl(startGameButton);



                var btn = new Button("Powr�t");
                btn.Bounds = new Rectangle(800, 650, 350, 50);
                btn.BackColor = Color.Black * 0.5f;
                btn.FrontColor = Color.White * 0.85f;
                btn.HoverColor = Color.White;
                btn.TextFont = ScreenManager.DefaultFontNormal;
                btn.ClickAction += ExitScreen;
                gui.AddControl(btn);


                playerSettingGui = new GUI(ScreenManager.Input);
                playerSettingGui.TextFont = gui.TextFont;

                var btn1 = new Button("Anuluj");
                btn1.Bounds = new Rectangle(700, 550, 200, 50);
                btn1.BackColor = Color.Black * 0.5f;
                btn1.FrontColor = Color.White * 0.85f;
                btn1.HoverColor = Color.White;
                btn1.TextFont = gui.TextFont;
                btn1.ClickAction += () =>
                {
                    ChangePlayer(false);
                };
                playerSettingGui.AddControl(btn1);


                var btn2 = new Button("Zapisz");
                btn2.Bounds = new Rectangle(950, 550, 200, 50);
                btn2.BackColor = Color.Black * 0.5f;
                btn2.FrontColor = Color.White * 0.85f;
                btn2.HoverColor = Color.White;
                btn2.TextFont = gui.TextFont;
                btn2.ClickAction += () =>
                {
                    ChangePlayer(true);
                };
                playerSettingGui.AddControl(btn2);

                var btn3 = new Button("Usu�");
                btn3.Bounds = new Rectangle(700, 450, 450, 50);
                btn3.BackColor = Color.Black * 0.5f;
                btn3.FrontColor = Color.White * 0.85f;
                btn3.HoverColor = Color.White;
                btn3.TextFont = gui.TextFont;
                btn3.ClickAction += RemoveSelectedPlayer;
                playerSettingGui.AddControl(btn3);


                playerNameInput = new Input();
                playerNameInput.TextFont = playerSettingGui.TextFont;
                playerNameInput.Bounds = new Rectangle(750, 100, 350, 50);
                playerNameInput.BackColor = Color.White * 0.25f;
                playerNameInput.FrontColor = Color.White;
                playerNameInput.IsActive = false;
                playerNameInput.CanWrite = true;
                playerSettingGui.AddControl(playerNameInput);

                moveUpButton = new Button(" U\nP");
                moveUpButton.Bounds = new Rectangle(615, 150, 70, 200);
                moveUpButton.BackColor = Color.Black * 0.5f;
                moveUpButton.FrontColor = Color.White * 0.85f;
                moveUpButton.HoverColor = Color.White;
                moveUpButton.TextFont = gui.TextFont;
                moveUpButton.ClickAction += MoveUp;
                playerSettingGui.AddControl(moveUpButton);


                moveDownButton = new Button("D\nO\nW\nN");
                moveDownButton.Bounds = new Rectangle(615, 400, 70, 200);
                moveDownButton.BackColor = Color.Black * 0.5f;
                moveDownButton.FrontColor = Color.White * 0.85f;
                moveDownButton.HoverColor = Color.White;
                moveDownButton.TextFont = gui.TextFont;
                moveDownButton.ClickAction += MoveDown;
                playerSettingGui.AddControl(moveDownButton);


                aiPlayerControlerGui = new GUI(ScreenManager.Input);
                aiPlayerControlerGui.TextFont = ScreenManager.DefaultFontNormal;

                easyAIButton = new Button("�atwy");
                easyAIButton.Bounds = new Rectangle(725, 200, 175, 50);
                easyAIButton.BackColor = Color.GreenYellow * 0.5f;
                easyAIButton.FrontColor = Color.White * 0.85f;
                easyAIButton.HoverColor = Color.GreenYellow;
                easyAIButton.TextFont = aiPlayerControlerGui.TextFont;
                easyAIButton.ClickAction += () => ChangeSelectedPlayerAILevel(AIControllers.Easy);

                aiPlayerControlerGui.AddControl(easyAIButton);

                hardAIButton = new Button("Trudny");
                hardAIButton.Bounds = new Rectangle(725 + 225, 200, 175, 50);
                hardAIButton.BackColor = Color.DarkRed * 0.5f;
                hardAIButton.FrontColor = Color.White * 0.85f;
                hardAIButton.HoverColor = Color.DarkRed;
                hardAIButton.TextFont = aiPlayerControlerGui.TextFont;
                hardAIButton.ClickAction += () => ChangeSelectedPlayerAILevel(AIControllers.Hard);


                aiPlayerControlerGui.AddControl(hardAIButton);
            }
        }

        private void ChangeSelectedPlayerAILevel(AIControllers level)
        {
            if(level == AIControllers.Easy)
            {
                easyAIButton.IsActive = false;
                easyAIButton.TextFont = ScreenManager.DefaultFontLarge;
                easyAIButton.BackColor = Color.GreenYellow;
                easyAIButton.FrontColor = Color.Black;

                hardAIButton.IsActive = true;
                hardAIButton.TextFont = aiPlayerControlerGui.TextFont;
                hardAIButton.BackColor = Color.DarkRed * 0.5f;
                hardAIButton.FrontColor = Color.White * 0.85f;
            }
            else if(level == AIControllers.Hard)
            {
                easyAIButton.IsActive = true;
                easyAIButton.TextFont = aiPlayerControlerGui.TextFont;
                easyAIButton.BackColor = Color.GreenYellow * 0.5f;
                easyAIButton.FrontColor = Color.White * 0.85f;
                hardAIButton.IsActive = false;
                hardAIButton.TextFont = ScreenManager.DefaultFontLarge;
                hardAIButton.BackColor = Color.DarkRed;
                hardAIButton.FrontColor = Color.Black;
            }
            selectedAIController = level;

        }

        private void RemoveSelectedPlayer()
        {
            if(selectedPlayerIndex != -1)
            {
                players.RemoveAt(selectedPlayerIndex);
                playerList.RemoveAt(selectedPlayerIndex);
                playerSettingEnable = false;
                playerNameInput.IsActive = false;
                selectedPlayerIndex = -1;


                if(playerList.Count >= 2)
                {
                    startGameButton.IsActive = true;
                    startGameButton.BackColor = Color.Green * 0.5f;
                }
                else
                {
                    startGameButton.IsActive = false;
                    startGameButton.BackColor = Color.Black * 0.25f;
                }
            }
        }

        private void StartGame()
        {
            LoadingScreen.Load(ScreenManager, true, new PlayScreen(playerList));
        }

        private void ChangePlayer(bool change)
        {
            if(change)
            {
                Logger.LogMessage($"Change name player {selectedPlayerIndex} from '{playerList[selectedPlayerIndex].Nickname}' to '{playerNameInput.Text}'");
                playerList[selectedPlayerIndex].Nickname = playerNameInput.Text;
                players.Items[selectedPlayerIndex].Text = playerNameInput.Text;
                if(selectedAIController == AIControllers.Easy)
                {
                    playerList[selectedPlayerIndex].Controller = new AIPlayerController(new EasyAIStrategy());
                }
                else if(selectedAIController == AIControllers.Hard)
                {
                    playerList[selectedPlayerIndex].Controller = new AIPlayerController(new HardAIStrategy());
                }
            }

            playerSettingEnable = false;
            playerNameInput.IsActive = false;
        }

        private void SwapListElements(List<ListItem> list, int first, int second)
        {
            var temp = list[first].Text;
            list[first].Text = list[second].Text;
            list[second].Text = temp;
        }

        private void SwapListElements(List<PlayerInfo> list, int first, int second)
        {
            var temp = new PlayerInfo(list[first]);
            list[first] = new PlayerInfo(list[second]);
            list[second] = new PlayerInfo(temp);
            //var temp = first.Clone();
            //first = second.Clone();
            //second = temp;
        }


        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Vector2 screenSize = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height);
            Vector2 center = screenSize / 2;

            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(Vector2.Zero.ToPoint(), screenSize.ToPoint()), Color.White);

            spriteBatch.Draw(spriteBatch.SolidTexture(), new Rectangle(75, 150, 525, 450), Color.White * 0.25f);

            for(int i = 1; i <= 6; i++)
            {
                spriteBatch.Draw(pawnTexture, new Vector2(125, 140 + (i * 75)), null, Player.GetPlayerColor(PlayerIndex.One + (i - 1)), 0, pawnTexture.Bounds.Size.ToVector2(), Vector2.One * 0.2f, SpriteEffects.None, 0);
            }




            gui.Draw(spriteBatch, gameTime, 1);



            if(playerSettingEnable)
            {
                spriteBatch.Draw(spriteBatch.SolidTexture(), new Rectangle(700, 50, 450, 350), Color.Black * 0.5f);

                playerSettingGui.Draw(spriteBatch, gameTime, 1);

                if(playerList[selectedPlayerIndex].Controller is AIPlayerController)
                {
                    aiPlayerControlerGui.Draw(spriteBatch, gameTime, 1f);
                }

            }

            string devBuild = "DEVELOPMENT BUILD <3";

            Vector2 devBuildSize = ScreenManager.DefaultFontSmall.MeasureString(devBuild);




            spriteBatch.DrawString(ScreenManager.DefaultFontSmall, devBuild, screenSize, Color.White, 0, devBuildSize, Vector2.One, SpriteEffects.None, 0);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if(State == eScreenState.Active)
            {

                gui.Update(gameTime);

                if(playerSettingEnable)
                {
                    playerSettingGui.Update(gameTime);

                    if(selectedPlayerIndex != -1 && selectedPlayerIndex < playerList.Count && playerList[selectedPlayerIndex].Controller is AIPlayerController)
                    {
                        aiPlayerControlerGui.Update(gameTime);
                    }
                }
            }
        }


        private void AddPlayer(bool isHuman)
        {
            string name = String.Empty;
            if(isHuman)
            {
                name = "human " + humanPlayers++;
                playerList.Add(new PlayerInfo()
                {
                    Nickname = name,
                    Controller = new HumanPlayerController()
                });
            }
            else
            {
                var randomVal = random.Next(Enum.GetValues(typeof(AIControllers)).Length);
                AIPlayerController controller = null;
                switch((AIControllers)randomVal)
                {
                    case AIControllers.Easy:
                    controller = new AIPlayerController(new EasyAIStrategy());
                    break;
                    case AIControllers.Hard:
                    controller = new AIPlayerController(new HardAIStrategy());
                    break;

                }

                name = "ai " + aiPlayers++;

                playerList.Add(new PlayerInfo()
                {
                    Nickname = name,
                    Controller = controller
                });

                //ToDo: Change forward Player object to PlayerInformation (nickname, controllerType, color, WhoFirst)
            }

            ListItem item = new ListItem(name);
            item.FrontColor = Color.White;
            item.BackColor = Color.Transparent;
            item.SelectedBackColor = Color.Black * 0.25f;
            item.TextFont = gui.TextFont;
            players.Add(item);
            item.ClickAction += ShowPlayerSettingPanel;
            if(playerList.Count >= 2)
            {
                startGameButton.IsActive = true;
                startGameButton.BackColor = Color.Green * 0.5f;

            }

            if(playerList.Count >= 6)
            {
                addAIButton.IsActive = false;
                addHumanButton.IsActive = false;
            }
        }

        private void MoveUp()
        {
            SwapListElements(players.Items, selectedPlayerIndex, selectedPlayerIndex - 1);
            SwapListElements(playerList, selectedPlayerIndex, selectedPlayerIndex - 1);
            selectedPlayerIndex--;
            players.SelectItem(selectedPlayerIndex);
            ShowPlayerSettingPanel(players.Items[selectedPlayerIndex]);
        }

        private void MoveDown()
        {
            SwapListElements(players.Items, selectedPlayerIndex, selectedPlayerIndex + 1);
            SwapListElements(playerList, selectedPlayerIndex, selectedPlayerIndex + 1);
            selectedPlayerIndex++;
            players.SelectItem(selectedPlayerIndex);
            ShowPlayerSettingPanel(players.Items[selectedPlayerIndex]);
        }

        private void UpdateButtons()
        {
            if(selectedPlayerIndex == 0)
            {
                moveUpButton.IsActive = false;
            }
            else
            {
                moveUpButton.IsActive = true;
            }

            if(selectedPlayerIndex == playerList.Count - 1)
            {
                moveDownButton.IsActive = false;
            }
            else
            {
                moveDownButton.IsActive = true;
            }
        }

        private void ShowPlayerSettingPanel(ListItem sender)
        {
            selectedPlayerIndex = players.SelectedItem;
            playerNameInput.Text = playerList[selectedPlayerIndex].Nickname;
            playerSettingEnable = true;
            playerNameInput.IsActive = true;
            UpdateButtons();

            players.Items[selectedPlayerIndex].Text = playerNameInput.Text;

            var pl = playerList[selectedPlayerIndex];

            if(pl.Controller is AIPlayerController)
            {
                var ai = pl.Controller as AIPlayerController;
                ChangeSelectedPlayerAILevel(ai.SelectedStrategy);
            }

        }
    }
}