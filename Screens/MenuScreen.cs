﻿using GameEngine.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine.GUI;
using Microsoft.Win32;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Do_not_get_angry_man.Screens
{
    class MenuScreen : GameScreen
    {
        private Texture2D background;
        private GUI gui;

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Vector2 screenSize = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height);
            Vector2 center = screenSize / 2;

            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(Vector2.Zero.ToPoint(), screenSize.ToPoint()), Color.White);

            string gameTitle = "Chińczyk";
            Vector2 titleSize = ScreenManager.DefaultFontLarge.MeasureString(gameTitle);

            spriteBatch.DrawString(ScreenManager.DefaultFontLarge, gameTitle, center - new Vector2(10), Color.White, 0, titleSize * new Vector2(0.5f), new Vector2(3f), SpriteEffects.None, 0);
            spriteBatch.DrawString(ScreenManager.DefaultFontLarge, gameTitle, center - new Vector2(7.5f), Color.White, 0, titleSize * new Vector2(0.5f), new Vector2(3f), SpriteEffects.None, 0);
            spriteBatch.DrawString(ScreenManager.DefaultFontLarge, gameTitle, center - new Vector2(5), Color.White, 0, titleSize * new Vector2(0.5f), new Vector2(3f), SpriteEffects.None, 0);
            spriteBatch.DrawString(ScreenManager.DefaultFontLarge, gameTitle, center - new Vector2(2.5f), Color.White, 0, titleSize * new Vector2(0.5f), new Vector2(3f), SpriteEffects.None, 0);
            spriteBatch.DrawString(ScreenManager.DefaultFontLarge, gameTitle, center, Color.Black, 0, titleSize * new Vector2(0.5f), new Vector2(3f), SpriteEffects.None, 0);
            gui.Draw(spriteBatch, gameTime, 1f);


            string devBuild = "DEVELOPMENT BUILD <3";

            Vector2 devBuildSize = ScreenManager.DefaultFontSmall.MeasureString(devBuild);

            spriteBatch.DrawString(ScreenManager.DefaultFontSmall, devBuild, screenSize, Color.White, 0, devBuildSize, Vector2.One, SpriteEffects.None, 0);

            spriteBatch.End();

        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
            if (State == eScreenState.Active && !coveredByOtherScreen)
            {
                gui.Update(gameTime);
            }
        }

        public MenuScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5f);
            TransitionOffTime = TimeSpan.FromSeconds(0.5f);
            CursorVisible = true;
        }

        public override void Activate(bool instancePreserved)
        {

            if (!instancePreserved)
            {
                background = ScreenManager.Content.Load<Texture2D>("background");
                gui = new GUI(ScreenManager.Input);
                gui.TextFont = ScreenManager.DefaultFontLarge;



                var button = new Button("Gra lokalna");
                button.TextFont = gui.TextFont;
                button.FrontColor = Color.White;
                button.HoverColor = Color.DarkGray;
                button.BackColor = Color.Transparent;
                button.Position = new Vector2(200, 400);
                button.ClickAction += StartGameButtonClick;
                button.IsActive = true;
                gui.AddControl(button);



                button = new Button("Gra sieciowa");
                button.TextFont = gui.TextFont;
                button.FrontColor = Color.White;
                button.HoverColor = Color.DarkGray;
                button.BackColor = Color.Transparent;
                button.Position = new Vector2(200, 475);
                button.ClickAction += EnterLobbyButtonClick;
                button.IsActive = false;
                gui.AddControl(button);


                button = new Button("Ustawienia");
                button.TextFont = gui.TextFont;
                button.FrontColor = Color.White;
                button.HoverColor = Color.DarkGray;
                button.BackColor = Color.Transparent;
                button.Position = new Vector2(800, 400);
                button.ClickAction += StartGameButtonClick;
                button.IsActive = false;
                gui.AddControl(button);


                button = new Button("Wyjście");
                button.TextFont = gui.TextFont;
                button.FrontColor = Color.White;
                button.HoverColor = Color.DarkGray;
                button.BackColor = Color.Transparent;
                button.Position = new Vector2(800, 475);
                button.ClickAction += () => { ScreenManager.Game.Exit(); };
                gui.AddControl(button);
            }
        }

        private void StartGameButtonClick()
        {
            ScreenManager.AddScreen(new LocalGameSettingsScreen());
        }

        private void EnterLobbyButtonClick()
        {
            ScreenManager.AddScreen(new LobbyScreen());
        }

        public override void Unload()
        {
            //background.Dispose();
        }
    }
}
