﻿using Microsoft.Xna.Framework;

namespace Do_not_get_angry_man
{
    public class PlayerInfo
    {
        public string Nickname;
        public bool IsFirstPlayer;
        public IPlayerController Controller;

        public PlayerInfo()
        {
                
        }

        public PlayerInfo(PlayerInfo pinfo)
        {
            this.Nickname = pinfo.Nickname;
            this.IsFirstPlayer = pinfo.IsFirstPlayer;
            this.Controller = pinfo.Controller;
        }
    }
    public class Player
    {
        private string nickname;
        private PlayerIndex playerIndex;
        private IPlayerController controller;


        public Player(string nickname, PlayerIndex index, IPlayerController controller)
        {
            this.nickname = nickname;
            this.playerIndex = index;
            this.controller = controller;
        }

        public Color Color
        {
            get { return playersColor[(int)playerIndex]; }
        }

        public static Color GetPlayerColor(PlayerIndex player)
        {
            return playersColor[(int)player];
        }
        private static Color[] playersColor = 
        {
            new Color(255, 0, 255),
            new Color(51, 91, 255),
            new Color(254, 254, 51),
            new Color(0, 255, 0),
            new Color(50, 50, 50),
            new Color(255, 0, 0)
        };

        internal int activePawns;
        internal int securePawns;
        public int ActivePawnsCount
        {
            get { return this.activePawns; }
        }

        public int SecurePawnsCount
        {
            get { return this.securePawns; }
        }

        public PlayerIndex Index
        {
            get { return this.playerIndex; }
        }

        public string Nickname
        {
            get { return this.nickname; }
            set { this.nickname = value; }
        }


        public override string ToString()
        {
            return string.Format("[{0}] {1}", ((int)playerIndex)+1, nickname);
        }

        public IPlayerController Controller
        {
            get { return this.controller; }
        }
    }
}