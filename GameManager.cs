﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Do_not_get_angry_man.Screens;
using GameEngine.Logger;
using GameEngine.Utilities;

namespace Do_not_get_angry_man
{
    public class GameManager
    {

        private GameBoard board;
        private Random random;
        private int actualPlayer = 1;

        private List<Player> playersList;


        private bool canRool;
        Dice dice;
        int roolNumer;

        private const int MaxRoolNumer = 3;
        private bool canPawnSelect;
        private bool pawnIsSelected;
        private Pawn selectedPawn;

        private bool moveDone = false;

        public GameManager(int firstPlayer = -1)
        {
            canRool = true;
            roolNumer = 0;
            random = new Random();
            actualPlayer = firstPlayer;
        }

        public GameBoard PrepareGameBoard(List<Player> playersList, GameBoard board)
        {
            if (actualPlayer == -1)
            {
                actualPlayer = random.Next(playersList.Count);
            }

            this.board = board;
            this.board.CreateBoard(playersList.Count);
            this.playersList = playersList;
            PreparePlayersPawns();
            return board;
        }

        private void PreparePlayersPawns()
        {
            foreach (Player player in playersList)
            {
                for (int i = 0; i < 4; i++)
                {
                    Pawn pawn = new Pawn(player, i);
                    board.AddPawn(pawn);
                }
            }
        }

        public Player CurentPlayerTurn
        {
            get
            {
                return playersList[actualPlayer];
            }
        }

        public void EndTurn()
        {
            actualPlayer = (actualPlayer + 1) % playersList.Count;
            canRool = true;
            roolNumer = 0;
            pawnIsSelected = false;
            selectedPawn = null;
            moveDone = false;
        }

        public bool CanRoolOfTheDice()
        {
            return this.canRool;
        }

        public void RoolOfTheDice()
        {
            if (canRool)
            {
                var diceNumber = dice.RoolADice(board.RecalculateLogicToPhysicPosition(board.GetHomeCenter(actualPlayer), board.Bounds.Size.ToVector2()), board.Bounds.Center.ToVector2());
                if (diceNumber != 6)
                {
                    if (CurentPlayerTurn.ActivePawnsCount > 0 && CurentPlayerTurn.ActivePawnsCount > CurentPlayerTurn.SecurePawnsCount)
                    {
                        canRool = false;
                    }
                    else
                    {
                        if (++roolNumer >= MaxRoolNumer)
                        {
                            canRool = false;
                        }
                    }
                }
                else
                {
                    roolNumer = MaxRoolNumer;
                }
                moveDone = false;
            }
        }

        internal bool CanMove(Pawn pawn)
        {
            if (moveDone == true)
            {
                return false;
            }
            if (pawn.IsActive)
            {
                int currentCellIndex = this.board.standardCells.IndexOf(pawn.cell);
                int targetCellIndex = (currentCellIndex + dice.Value) % this.board.standardCells.Count;



                Cell playerStartCell = this.board.standardCells.Find(c => c.Type == Cell.cellType.StartPoint && c.PlayerIndex == pawn.Player.Index);

                Debug.Assert(playerStartCell != null, "Nie znaleziono punktu startowego");

                int startCellIndex = this.board.standardCells.IndexOf(playerStartCell);



                if (pawn.IsSafe)
                {
                    currentCellIndex = this.board.hideAwaysCells.IndexOf(pawn.cell);
                    targetCellIndex = currentCellIndex + dice.Value;
                    if (targetCellIndex >= this.board.hideAwaysCells.Count)
                    {
                        return false;
                    }
                    else
                    {
                        Cell targerHideAwayCell = board.hideAwaysCells[targetCellIndex];
                        if (targerHideAwayCell.PlayerIndex == pawn.Player.Index)
                        {
                            return pawn.CanMove(targerHideAwayCell);
                        }
                        return false;
                    }
                }


                if (targetCellIndex >= startCellIndex &&
                    ((currentCellIndex > startCellIndex && currentCellIndex > targetCellIndex) ||
                     (currentCellIndex < startCellIndex && currentCellIndex < targetCellIndex)))
                {

                    var hideAwayCellIndex = (targetCellIndex - startCellIndex) + (int)pawn.Player.Index * 4;
                    if (hideAwayCellIndex >= this.board.hideAwaysCells.Count)
                    {
                        return false;
                    }

                    Cell targerHideAwayCell = board.hideAwaysCells[hideAwayCellIndex];
                    Debug.Assert(targerHideAwayCell != null, "Nie znaleziono miejsca w kryjówce");

                    if (targerHideAwayCell.PlayerIndex == pawn.Player.Index)
                    {
                        return pawn.CanMove(targerHideAwayCell);
                    }
                    return false;

                    //
                    // idziemy do kryjówki
                }
                else
                {
                    Cell targerCell = board.standardCells[targetCellIndex];
                    return pawn.CanMove(targerCell);
                    // chodzimy dalej po planszy
                }
            }
            else
            {
                if (Dice == 6)
                {
                    Cell playerStartCell = this.board.standardCells.Find(c => c.Type == Cell.cellType.StartPoint && c.PlayerIndex == pawn.Player.Index);
                    Debug.Assert(playerStartCell != null, "Nie znaleziono punktu startowego");
                    return pawn.CanMove(playerStartCell);
                }
                return false;
            }

        }

        internal bool PawnHitOtherPlayerPawn(Pawn pawn)
        {
            if (pawn.IsActive)
            {
                int currentCellIndex = this.board.standardCells.IndexOf(pawn.cell);
                int targetCellIndex = (currentCellIndex + dice.Value) % this.board.standardCells.Count;

                return (!board.standardCells[targetCellIndex].Empty &&
                        board.standardCells[targetCellIndex].content.Player != pawn.Player);
            }
            return false;
        }

        internal bool PawnEnterToHideaway(Pawn pawn, out int distance)
        {
            if (pawn.IsActive && !pawn.IsSafe)
            {
                distance = pawn.DistanceToEnd - Dice;
                if (distance >= 4|| distance < 0)
                    return false;

                var playerHideaways = board.GetPlayerHideaways(pawn.Player);
                var targetHideawayCell = playerHideaways[(playerHideaways.Count-1) - distance];
                return pawn.CanMove(targetHideawayCell);

            }
            distance = int.MaxValue;
            return false;
        }

        public int Dice
        {
            get
            {
                return this.dice.Value;
            }
        }

        public bool PawnIsSelected

        {
            get { return this.pawnIsSelected; }
        }

        public bool CanMove()
        {
            if (PawnIsSelected)
            {
                return CanMove(selectedPawn);
            }
            return false;
        }

        public void Move()
        {
            if (CanMove())
            {
                if (selectedPawn.IsActive)
                {
                    int currentCellIndex = this.board.standardCells.IndexOf(selectedPawn.cell);
                    int targetCellIndex = (currentCellIndex + dice.Value) % this.board.standardCells.Count;

                    Logger.LogMessage($"Current cell Index {currentCellIndex}");
                    Logger.LogMessage($"Target cell Index {targetCellIndex}");
                    Cell playerStartCell = this.board.standardCells.Find(c => c.Type == Cell.cellType.StartPoint && c.PlayerIndex == selectedPawn.Player.Index);

                    Debug.Assert(playerStartCell != null, "Nie znaleziono punktu startowego");

                    int startCellIndex = this.board.standardCells.IndexOf(playerStartCell);

                    Logger.LogMessage($"Start Cell Index {startCellIndex}");
                    Logger.LogMessage(selectedPawn.ToString());

                    if (selectedPawn.IsSafe)
                    {
                        currentCellIndex = this.board.hideAwaysCells.IndexOf(selectedPawn.cell);
                        targetCellIndex = currentCellIndex + dice.Value;

                        Cell targetHideawayCell = board.hideAwaysCells[targetCellIndex];

                        Logger.LogMessage("Movment safe pawn");
                        Logger.LogMessage($"Target Hideaway Cell { targetCellIndex}");

                        selectedPawn.Move(targetHideawayCell);
                        moveDone = true;
                        return;
                    }

                    if (targetCellIndex >= startCellIndex &&
                    ((currentCellIndex > startCellIndex && currentCellIndex > targetCellIndex) ||
                    (currentCellIndex < startCellIndex && currentCellIndex < targetCellIndex)))
                    {

                        var hideAwayCellIndex = (targetCellIndex - startCellIndex) + (int)selectedPawn.Player.Index * 4;


                        Cell targerHideAwayCell = board.hideAwaysCells[hideAwayCellIndex];
                        Debug.Assert(targerHideAwayCell != null, "Nie znaleziono miejsca w kryjówce");

                        Logger.LogMessage("Movment pawn to hideaway");
                        Logger.LogMessage($"Target Hideaway Cell { hideAwayCellIndex}");

                        selectedPawn.Move(targerHideAwayCell);
                        selectedPawn.Secure();
                        moveDone = true;
                    }
                    else
                    {
                        Cell targerCell = board.standardCells[targetCellIndex];

                        // tutaj załatwić bicie
                        if (!targerCell.Empty)
                        {
                            Pawn pawnToKill = targerCell.content;

                            Cell homeCell = board.homeCells.Find(c => c.Empty && c.PlayerIndex == pawnToKill.Player.Index);

                            Debug.Assert(homeCell != null, "Brak miejsca w domu przy zbiciu pionka");


                            Logger.LogMessage($"Movment pawn to ocupated cell, {pawnToKill} killed");
                            Logger.LogMessage($"Home Cell index { board.homeCells.IndexOf(homeCell) }");


                            pawnToKill.Move(homeCell);
                            pawnToKill.UnActivate();

                        }

                        selectedPawn.Move(targerCell);
                        moveDone = true;
                    }
                }
                else
                {
                    if (Dice == 6)
                    {
                        Cell playerStartCell = this.board.standardCells.Find(c => c.Type == Cell.cellType.StartPoint && c.PlayerIndex == selectedPawn.Player.Index);
                        Debug.Assert(playerStartCell != null, "Nie znaleziono punktu startowego");

                        // tutaj załatwić bicie
                        if (!playerStartCell.Empty)
                        {
                            Pawn pawnToKill = playerStartCell.content;

                            Cell homeCell = board.homeCells.Find(c => c.Empty && c.PlayerIndex == pawnToKill.Player.Index);

                            Debug.Assert(homeCell != null, "Brak miejsca w domu przy zbiciu pionka");

                            Logger.LogMessage($"Player start cell is ocupated, pawn {pawnToKill} killed");
                            Logger.LogMessage($"Home Cell index{ board.homeCells.IndexOf(homeCell) }");

                            pawnToKill.Move(homeCell);
                            pawnToKill.UnActivate();

                        }
                        selectedPawn.Activate();
                        selectedPawn.Move(playerStartCell);

                        Logger.LogMessage($"{selectedPawn} set acivate, and movment to cell index {board.standardCells.IndexOf(playerStartCell)}");
                        moveDone = true;
                    }
                }
                pawnIsSelected = false;
                selectedPawn = null;

                DumpBoard();

            }
        }

        public bool IsAvaliableMovement()
        {
            {
                Pawn tempSelectedPawn = selectedPawn;
                bool tempIsSelectedPawn = pawnIsSelected;

                bool isAvaliableMovement = false;

                var avaliablePawns = board.GetAllPlayerPawns(playersList[actualPlayer]);
                pawnIsSelected = true;
                foreach (Pawn pawn in avaliablePawns)
                {
                    selectedPawn = pawn;
                    if (CanMove())
                    {
                        isAvaliableMovement = true;

                    }
                }
                selectedPawn = tempSelectedPawn;
                pawnIsSelected = tempIsSelectedPawn;
                return isAvaliableMovement;
            }
        }

        public bool CanSelectPawn
        {
            get
            {
                return this.canPawnSelect;
            }
            set
            {
                this.canPawnSelect = value;
                this.pawnIsSelected = false;
                this.selectedPawn = null;
            }
        }

        public void SetDice(Dice die)
        {
            this.dice = die;
        }

        public void SelectPawn(Pawn pawn)
        {
            if (pawn == null)
            {
                this.selectedPawn = pawn;
                this.pawnIsSelected = false;
            }
            else
            {
                this.selectedPawn = pawn;
                this.pawnIsSelected = true;
            }
        }

        public bool MoveDone
        {
            get { return this.moveDone; }
        }

        public bool PawnCanBeHitByOtherPawn(Pawn target, Pawn source, out int distance)
        {
            distance = DistanceBetweenPawns(source, target);
            return distance < source.DistanceToEnd;
        }
        public void DumpBoard()
        {
            Logger.LogWarning("start dump pawns position");
            foreach (Player player in playersList)
            {
                var pawns = board.GetAllPlayerPawns(player);

                Logger.LogMessage(player.Nickname + " pawns");
                foreach (Pawn pawn in pawns)
                {
                    //string logText = string.Empty;// = ;

                    //var cell = pawn.cell;
                    //if (cell != null)
                    //{
                    //    if (cell.Type == Cell.cellType.Home)
                    //    {
                    //        logText = "Home cell index " + board.homeCells.IndexOf(cell);
                    //    }
                    //    else if (cell.Type == Cell.cellType.Hideaway)
                    //    {
                    //        logText = "Hideaway cell index " + board.hideAwaysCells.IndexOf(cell);
                    //    }
                    //    else
                    //    {
                    //        logText = "Standard cell index " + board.standardCells.IndexOf(cell);
                    //    }
                    //}
                    //Logger.LogMessage($"{pawn.ToString()} {logText,-25}");
                    Logger.LogMessage($"Pawn player {pawn.Player.Index} id {pawn.ID} distance to end {pawn.DistanceToEnd} ");
                }
            }
            Logger.LogWarning("end dump pawns position");
        }

        public int DistanceBetweenPawns(Pawn first, Pawn second)
        {
            var firstCell = first.cell;
            var secondCell = second.cell;
            return board.standardCells.Distance(firstCell, secondCell);
        }
    }
}