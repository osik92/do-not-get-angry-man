﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using GameEngine.Logger;

namespace Do_not_get_angry_man
{
    public class AIPlayerController : IPlayerController
    {
        public event OnPawnSelectHandler OnPawnSelect;

        private IAIStrategy aiStrategy;
        public AIPlayerController(IAIStrategy aiStrategy)
        {
            this.aiStrategy = aiStrategy;
        }

        public bool IsEndTurn()
        {
            this.aiStrategy.TurnEnd();
            return true;
        }

        public bool IsRollOfDice()
        {
            return true;
        }

        public void Action(GameManager manager, GameBoard board)
        {
            this.aiStrategy.Action(OnPawnSelect, manager, board);
        }

        public AIControllers SelectedStrategy
        {
            get
            {
                if(this.aiStrategy is EasyAIStrategy)
                    return AIControllers.Easy;
                if(this.aiStrategy is HardAIStrategy)
                    return AIControllers.Hard;
                return AIControllers.Easy;
            }
        }
    }

    public enum AIControllers
    {
        Easy = 0,
        Hard
    }

    public interface IAIStrategy
    {
        void Action(OnPawnSelectHandler pawnSelectedAction, GameManager manager, GameBoard board);
        void TurnEnd();
    }

    public class EasyAIStrategy : IAIStrategy
    {
        private int id = 0;
        public void Action(OnPawnSelectHandler pawnSelectedAction, GameManager manager, GameBoard board)
        {
            var myPawns = board.GetAllPlayerPawns(manager.CurentPlayerTurn);
            var pawn = myPawns.Find(p => p.ID.Equals(id));

            pawnSelectedAction?.Invoke(pawn);
            id = ++id % myPawns.Count;
        }

        public void TurnEnd()
        {
            id = 0;
        }
    }

    public class HardAIStrategy : IAIStrategy
    {
        public void Action(OnPawnSelectHandler pawnSelectedAction, GameManager manager, GameBoard board)
        {
            //Logger.LogMessage("[HardAIStrategy] Action");
            var myPawns = board.GetAllPlayerPawns(manager.CurentPlayerTurn);

            var myActivePawns = myPawns.FindAll(p => p.IsActive == true);
            //ToDo: Dokończyć pisanie mądrej sztucznej inteligencji 

            Pawn selectedPawn = null;


            if(manager.Dice == 6 && myActivePawns.Count != 4)
            {
                foreach(Pawn notActivePawn in myPawns.FindAll(p => p.IsActive == false))
                {
                    if(manager.CanMove(notActivePawn))
                    {
                        selectedPawn = notActivePawn;
                        pawnSelectedAction?.Invoke(selectedPawn);
                        return;
                    }
                }
            }

            float bestScore = float.MinValue;
            foreach(Pawn pawn in myActivePawns)
            {
                var value = CalculateTurn(manager, board, pawn);
                //Logger.LogMessage($"[HardAIStrategy] calucaltedValue for {pawn} is {value}");
                if(value > bestScore && manager.CanMove(pawn))
                {
                    bestScore = value;
                    selectedPawn = pawn;
                }
            }
            pawnSelectedAction?.Invoke(selectedPawn);
        }

        public void TurnEnd()
        {
        }

        private float CalculateTurn(GameManager manager, GameBoard board, Pawn pawn)
        {
            int killedPlayer = 0;
            float enterToHideawayInNextTurn = 0f;
            int enterToHideaway = 0;
            int pawnsWhoCanKilledMeInNextTurn = 0;
            int pawnsWhoCanKilledMeAnyTime = 0;
            int pawnIsSafe = pawn.IsSafe ? 1 : 0;
            float totalCellCount = (board.BoardCells + 3);
            float distanceToEndBonus = (pawn.DistanceToEnd) / totalCellCount;



            var enemyPawns = board.GetAllPawns()
                .FindAll(p => p.Player != pawn.Player && p.IsActive == true && p.IsSafe == false);

            foreach(Pawn enemyPawn in enemyPawns)
            {
                int distance;
                if(manager.PawnCanBeHitByOtherPawn(pawn, enemyPawn, out distance))
                {
                    pawnsWhoCanKilledMeAnyTime++;
                }
                if(distance <= 6)
                {
                    pawnsWhoCanKilledMeInNextTurn++;
                }
            }

            pawnsWhoCanKilledMeAnyTime -= pawnsWhoCanKilledMeInNextTurn;

            int dist;
            killedPlayer = manager.PawnHitOtherPlayerPawn(pawn) ? 1 : 0;
            enterToHideaway = manager.PawnEnterToHideaway(pawn, out dist) ? 1 : 0;

            float propabilityEnterToHideawayOnNextTurn = 0;
            if(dist <= 10)
            {
                if(dist <= 4)
                {
                    propabilityEnterToHideawayOnNextTurn =
                        (float)board.GetPlayerHideaways(pawn.Player).FindAll(c => !c.Empty).Count / 6f;
                }
                else
                {
                    propabilityEnterToHideawayOnNextTurn =
                        (float)board.GetPlayerHideaways(pawn.Player).FindAll(c => !c.Empty).Count - (dist - 2) / 6f;
                }
            }

            return (killedPlayer * 7) + (propabilityEnterToHideawayOnNextTurn * 10) + (enterToHideaway * 12) + distanceToEndBonus * 2
                + ((pawnsWhoCanKilledMeInNextTurn * 2.5f) / 6f) + ((pawnsWhoCanKilledMeAnyTime * 1.5f) / 6f) - pawnIsSafe * 10;
        }
    }

}