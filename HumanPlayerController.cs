﻿namespace Do_not_get_angry_man
{
    public class HumanPlayerController : IPlayerController
    {
        private bool endTurn = false;
        private bool rollOfDice = false;

        public event OnPawnSelectHandler OnPawnSelect;

        public bool IsEndTurn()
        {
            bool temp = endTurn;
            if(temp)
            {
                endTurn = !endTurn;
            }
            return temp;
        }

        public bool IsRollOfDice()
        {
            bool temp = rollOfDice;
            if (temp)
            {
                rollOfDice = !rollOfDice;
            }
            return temp;
        }

        public void EndTurn()
        {
            this.endTurn = true;
        }
        public void RollOfDice()
        {
            this.rollOfDice = true;
        }

        public void PawnSelect(Pawn pawn)
        {
            OnPawnSelect?.Invoke(pawn);
        }
        
    }
}