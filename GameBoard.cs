﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using MathL = GameEngine.Utilities.Math;
using MathS = System.Math;
using System;
using GameEngine;
using System.Diagnostics;

namespace Do_not_get_angry_man
{
    public class GameBoard
    {
        internal List<Cell> standardCells;
        internal List<Cell> hideAwaysCells;
        internal List<Cell> homeCells;


        private List<Pawn> playersPawns;


        public Texture2D cellImage;
        public Texture2D pawnImage;
        public Texture2D hatImage;
        public Texture2D ballonImage;
        public bool drawHat;
        public bool drawBallon;

        private Vector2 position;
        private Vector2 size;

        Vector2 cellSize;

        private Vector2[] PlayersHomeCenter;
        public bool sort = true;



        public OnPawnSelectHandler OnPawnSelect;
        public Dice dice;


        public GameBoard()
        {
            standardCells = new List<Cell>();
            hideAwaysCells = new List<Cell>();
            homeCells = new List<Cell>();
            playersPawns = new List<Pawn>();
        }

        public Rectangle Bounds
        {
            set
            {
                this.size = value.Size.ToVector2();
                this.position = value.Location.ToVector2();
                cellSize = size / 30.0f;
            }
            get
            {
                return new Rectangle(this.position.ToPoint(), this.size.ToPoint());
            }
        }
        internal void AddPawn(Pawn pawn)
        {
            pawn.board = this;
            Cell cell = homeCells.Find(C => C.PlayerIndex == pawn.Player.Index && C.Empty);
            if (cell != null)
            {
                cell.content = pawn;
                playersPawns.Add(pawn);
                pawn.Position = position + RecalculateLogicToPhysicPosition(cell.Position, size);
                pawn.Size = cellSize * new Vector2(1.0f, 2.0f);
                pawn.texture = pawnImage;
                pawn.shadow = cellImage;
                pawn.hat = hatImage;
                pawn.ballon = ballonImage;
                pawn.drawBallon = drawBallon;
                pawn.drawHat = drawHat;
                pawn.cell = cell;
            }
        }
        public void CreateBoard(int playerCount)
        {
            float m = 5;
            float angle = 360.0f / playerCount;

            PlayersHomeCenter = new Vector2[playerCount];

            float outerCircleRadius = 0.425f;
            float innerCircleRadius = outerCircleRadius / 2.0f;

            Vector2 CenterPoint = new Vector2(0.5f, 0.5f);

            for (int i = 0; i < playerCount; ++i)
            {
                Vector2 firstPoint = new Vector2((float)MathS.Sin(MathHelper.ToRadians(i * angle)) * outerCircleRadius + CenterPoint.X, -(float)MathS.Cos(MathHelper.ToRadians(i * angle)) * outerCircleRadius + CenterPoint.Y);
                Vector2 secondPoint = new Vector2((float)MathS.Sin(MathHelper.ToRadians(i * angle - (angle / 2.0f))) * innerCircleRadius + CenterPoint.X, -(float)MathS.Cos(MathHelper.ToRadians(i * angle - (angle / 2.0f))) * innerCircleRadius + CenterPoint.Y);


                Vector2 distance = (firstPoint - secondPoint) / m;

                for (int j = 1; j <= m - 1; ++j)
                {
                    standardCells.Add(new Cell(secondPoint + (distance * j)));
                }

                standardCells.Add(new Cell(firstPoint, Cell.cellType.StartPoint, i));


                secondPoint = new Vector2((float)MathS.Sin(MathHelper.ToRadians(i * angle + (angle / 2.0f))) * innerCircleRadius + CenterPoint.X, -(float)MathS.Cos(MathHelper.ToRadians(i * angle + (angle / 2.0f))) * innerCircleRadius + CenterPoint.Y);
                distance = (secondPoint - firstPoint) / m;

                for (int j = 1; j <= m - 1; ++j)
                {
                    standardCells.Add(new Cell(firstPoint + (distance * j)));
                }

                standardCells.Add(new Cell(secondPoint));

                Vector2 distanceToCenter = (firstPoint - CenterPoint) / 7.0f;

                for (int j = 2; j <= 5; ++j)
                {
                    this.hideAwaysCells.Add(new Cell(firstPoint - (distanceToCenter * j), Cell.cellType.Hideaway, i));
                }

                Vector2 homeCenter = new Vector2((float)MathS.Sin(MathHelper.ToRadians(i * angle + (angle / 3))) * outerCircleRadius * 0.95f + CenterPoint.X, -(float)MathS.Cos(MathHelper.ToRadians(i * angle + (angle / 3))) * outerCircleRadius * 0.95f + CenterPoint.Y);
                PlayersHomeCenter[i] = homeCenter;

                homeCells.Add(new Cell(homeCenter + new Vector2(-0.01f, -0.01f), Cell.cellType.Home, i));
                homeCells.Add(new Cell(homeCenter + new Vector2(+0.03f, -0.01f), Cell.cellType.Home, i));
                homeCells.Add(new Cell(homeCenter + new Vector2(-0.02f, +0.03f), Cell.cellType.Home, i));
                homeCells.Add(new Cell(homeCenter + new Vector2(+0.02f, +0.03f), Cell.cellType.Home, i));
            }
        }
        public void Update(InputHandler input, GameTime gameTime)
        {
            Rectangle bounds = new Rectangle(position.ToPoint(), size.ToPoint());

            if (bounds.Contains(input.GetMousePosition()))
            {
                if (sort)
                {
                    playersPawns.Sort((p1, p2) => p1.Position.Y < p2.Position.Y ? 1 : ((Math.Abs(p1.Position.Y - p2.Position.Y) < 0.001f) ? 0 : -1));
                    //playersPawns.Sort((p1, p2) => p1.Position.Y < p2.Position.Y ? 1 : -1);
                }

                bool anyPawnIsHovered = false;

                foreach (Pawn pawn in playersPawns)
                {
                    if (pawn.Bounds.Contains(input.GetMousePosition()) && !anyPawnIsHovered)
                    {
                        pawn.OnHover();
                        anyPawnIsHovered = true;

                        if (input.IsMouseButtonUp(GameEngine.Input.eMouseButton.Left))
                        {
                            OnPawnSelect?.Invoke(pawn);
                        }
                    }
                    else
                    {
                        pawn.OffHover();
                    }
                }
            }
            playersPawns.Sort((p1, p2) => p1.Position.Y < p2.Position.Y ? -1 : ((Math.Abs(p1.Position.Y - p2.Position.Y) < 0.001f) ? 0 : 1));
            //playersPawns.Sort((p1, p2) => p1.Position.Y < p2.Position.Y ? -1 : 1);
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(cellImage, new Rectangle(position.ToPoint(), size.ToPoint()), Color.Black * 0.5f);

            for (int i = 0; i < standardCells.Count; ++i)
            {
                Vector2 cellPosition = position + RecalculateLogicToPhysicPosition(standardCells[i].Position, size);

                Color color = Color.White;
                if (standardCells[i].Type == Cell.cellType.StartPoint)
                {
                    color = Player.GetPlayerColor((PlayerIndex)standardCells[i].PlayerIndex);
                }



                spriteBatch.Draw(cellImage, new Rectangle(cellPosition.ToPoint(), cellSize.ToPoint()), null, color, 0f, cellImage.Bounds.Center.ToVector2(), SpriteEffects.None, 0);
            }

            for (int i = 0; i < hideAwaysCells.Count; ++i)
            {
                Vector2 cellPosition = position + RecalculateLogicToPhysicPosition(hideAwaysCells[i].Position, size);

                Color color = Player.GetPlayerColor((PlayerIndex)hideAwaysCells[i].PlayerIndex);
                spriteBatch.Draw(cellImage, new Rectangle(cellPosition.ToPoint(), cellSize.ToPoint()), null, color, 0f, cellImage.Bounds.Center.ToVector2(), SpriteEffects.None, 0);
            }

            for (int i = 0; i < homeCells.Count; ++i)
            {
                Vector2 cellPosition = position + RecalculateLogicToPhysicPosition(homeCells[i].Position, size);

                Color color = Player.GetPlayerColor((PlayerIndex)homeCells[i].PlayerIndex);
                spriteBatch.Draw(cellImage, new Rectangle(cellPosition.ToPoint(), cellSize.ToPoint()), null, color, 0f, cellImage.Bounds.Center.ToVector2(), SpriteEffects.None, 0);
            }



            // Drawing pawns


            //for (int i = 0; i < homeCells.Count; ++i)
            //{
            //    if (!homeCells[i].Empty)
            //    {
            //        Vector2 cellPosition = position + RecalculateLogicToPhysicPosition(homeCells[i].Position, size);

            //        Color color = homeCells[i].content.Player.Color;
            //        spriteBatch.Draw(pawnImage, new Rectangle(cellPosition.ToPoint(), new Point((int)cellSize.X, (int)cellSize.Y * 2)), null, color, 0f, new Vector2(0.5f, 0.8f) * pawnImage.Bounds.Size.ToVector2(), SpriteEffects.None, 0);
            //    }
            //}

            List<Object> tempSortList = new List<object>(playersPawns);

            tempSortList.Add(dice);

            tempSortList.Sort((o1, o2) =>
           {

               if (o1 is Pawn)
               {
                   if (o2 is Pawn)
                   {
                       return ((Pawn)o1).Position.Y < ((Pawn)o2).Position.Y ? -1 : 1;
                   }
                   else
                   {
                       return ((Pawn)o1).Position.Y < ((Dice)o2).Position.Y ? -1 : 1;
                   }
               }
               else
               {
                   return ((Dice)o1).Position.Y < ((Pawn)o2).Position.Y ? -1 : 1;
               }

           });

            foreach (var obj in tempSortList)
            {
                if (obj is Pawn)
                {
                    ((Pawn)obj).Draw(spriteBatch, gameTime);
                }
                else
                {
                    ((Dice)obj).Draw(spriteBatch);
                }
            }

        }

        public List<Pawn> GetAllPlayerPawns(Player player)
        {
            return playersPawns.FindAll(p => p.Player.Equals(player));
        }


        public List<Pawn> GetAllPawns()
        {
            return playersPawns;
        }

        public Vector2 RecalculateLogicToPhysicPosition(Vector2 logicPosition, Vector2 physicSize)
        {
            Vector2 recalculatePositon = new Vector2(MathL.Map(logicPosition.X, 0, 1, 0, physicSize.X), MathL.Map(logicPosition.Y, 0, 1, 0, physicSize.Y));
            return recalculatePositon;
        }

        public Vector2 GetHomeCenter(int player)
        {
            return PlayersHomeCenter[player];
        }

        public List<Cell> GetPlayerHideaways(Player player)
        {
            return  hideAwaysCells.GetRange((int)player.Index * 4, 4);
        }

        public int BoardCells
        {
            get { return this.standardCells.Count; }
        }

    }
}
