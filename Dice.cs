﻿using GameEngine.GUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using GameEngine.Logger;

namespace Do_not_get_angry_man
{
    public class Dice
    {
        public SimpleImagesAtlas dices;
        private Random random;
        private float rotation;

        private float animationDuration;
        private TimeSpan animationTime;


        private Vector2 startPosition;
        private Vector2 endPosition;
        private Vector2 position;

        private Color color;

        private int dice;

        private enum AnimationState
        {
            Init,
            RoolDiceAnimation,
            ShowStaticImage
        }
        private AnimationState animationState;

        public Dice(float animationDuration)
        {
            int seed = Environment.TickCount;
            //int seed = 10402015;
            this.random = new Random(seed);
            Logger.LogMessage($"Create dice, random seed is {seed}");
            this.animationDuration = animationDuration;
            this.animationState = AnimationState.Init;
        }

        public void Update(GameTime gameTime)
        {
            if(animationState == AnimationState.RoolDiceAnimation)
            {
                animationTime -= gameTime.ElapsedGameTime;
                var t = (animationDuration - (float)animationTime.TotalSeconds) / animationDuration;
                position = Vector2.Lerp(startPosition, endPosition, t);
                color = Color.Lerp(Color.Black * 0f, Color.White, t);
                rotation = MathHelper.Lerp( MathHelper.ToRadians((startPosition.X > endPosition.X) ? (360 * 2) : -(360 * 2)), 0, t);
                if (animationTime <= TimeSpan.Zero)
                {
                    animationState = AnimationState.ShowStaticImage;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (dices == null)
                return;
            switch (animationState)
            {
                case AnimationState.RoolDiceAnimation:

                    dices.Draw(spriteBatch, new Rectangle((int)position.X, (int)position.Y, 388 / 6, 425 / 6), color, rotation, new Vector2(0.5f, 0.5f), SpriteEffects.None, 0, (dice -1));

                    break;
                case AnimationState.ShowStaticImage:

                    dices.Draw(spriteBatch, new Rectangle((int)position.X, (int)position.Y, 388 / 6, 425 / 6), color, rotation, new Vector2(0.5f, 0.5f), SpriteEffects.None, 0, 6 + (dice - 1));

                    break;
            }
        }

        public int RoolADice(Vector2 startPosition, Vector2 endPosition)
        {
            this.animationState = AnimationState.RoolDiceAnimation;
            this.animationTime = TimeSpan.FromSeconds(animationDuration);


            this.startPosition = startPosition;
            this.endPosition = endPosition;

            dice = random.Next(6) + 1;
            Logger.LogMessage($"Rool a Dice {dice}");
            return dice;
        }

        public void Stop()
        {
            this.animationState = AnimationState.Init;
        }

        public int Value
        {
            get { return this.dice; }
        }

        public Vector2 Position
        {
            get { return this.position; }
        }

        public bool AnimationEnd
        {
            get { return animationState == AnimationState.ShowStaticImage; }
        }

    }

}