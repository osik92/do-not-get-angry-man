﻿using Microsoft.Xna.Framework;


namespace Do_not_get_angry_man
{
    public class Cell
    {
        Vector2 position;

        public enum cellType
        {
            Standard,
            StartPoint,
            Hideaway,
            Home
        }

        cellType type;

        internal Pawn content;
        private PlayerIndex playerIndex;
        

        public bool Empty
        {
            get { return content == null; }
            
        }

        public Cell(Vector2 position) : this(position, cellType.Standard)
        {

        }

        public Cell(Vector2 position, cellType type, int playerIndex = -1)
        {
            this.type = type;
            this.position = position;
            this.playerIndex = (PlayerIndex) playerIndex;
        }

        public Vector2 Position
        {
            get { return position; }
        }

        public cellType Type
        {
            get
            {
                return this.type;
            }
        }

        public PlayerIndex PlayerIndex
        {
            get
            {
                return this.playerIndex;
            }
        }
    }


}
