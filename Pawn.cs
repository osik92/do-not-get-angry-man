﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using GameEngine.Utilities;

namespace Do_not_get_angry_man
{
    public class Pawn
    {
        private Player owner;
        internal Cell cell;
        internal GameBoard board;
        private bool isActive;
        private bool isSafe;
        private int distanceToEnd = Int32.MaxValue;

        internal Vector2 Position;
        internal Vector2 Size;
        internal Texture2D texture;
        internal Texture2D shadow;
        internal Texture2D hat;
        internal Texture2D ballon;
        internal bool isSelected = false;
        internal bool isHovered = false;


        internal bool drawHat = false;
        internal bool drawBallon = false;

        private Vector2 offset = Vector2.Zero;
        Vector2 scale = Vector2.One;

        private int id;


        public Pawn(Player player, int id)
        {
            this.owner = player;
            this.isActive = false;
            this.isSafe = false;
            this.id = id;
        }

        public bool CanMove(Cell cell)
        {
            if (cell.Empty || cell.content.Player != owner)
            {
                return true;
            }
            return false;
        }

        public void Move(Cell cell)
        {
            if (cell.Type == Cell.cellType.Hideaway)
            {
                DistanceToEnd = 4 - (board.GetPlayerHideaways(this.Player).IndexOf(cell) + 1 );
            }
            else if ((cell.Type == Cell.cellType.StartPoint && cell.PlayerIndex == this.Player.Index) || cell.Type == Cell.cellType.Home)
            {
                //nothing
            }
            else
            {
                DistanceToEnd -= board.standardCells.Distance<Cell>(this.cell, cell);
            }

            this.cell.content = null;
            this.Position = board.Bounds.Location.ToVector2() + board.RecalculateLogicToPhysicPosition(cell.Position, board.Bounds.Size.ToVector2());
            this.cell = cell;
            cell.content = this;
        }

        public void Activate()
        {
            this.Player.activePawns++;
            this.isActive = true;
            this.DistanceToEnd = board.BoardCells + 3;

        }

        public void UnActivate()
        {
            this.isActive = false;
            this.Player.activePawns--;
            this.DistanceToEnd = int.MaxValue;
        }

        public Player Player
        {
            get { return this.owner; }
        }


        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (isSelected)
            {
                spriteBatch.Draw(shadow, new Rectangle((Position).ToPoint(), new Vector2(Size.X, Size.X / 2).ToPoint()), null, Color.Black * 0.85f, 0, shadow.Bounds.Center.ToVector2(), SpriteEffects.None, 0);

            }

            if (ballon != null && drawBallon)
            {
                spriteBatch.Draw(ballon, new Rectangle((Position + offset).ToPoint(), (Size * (scale * 1.5f)).ToPoint()), null, Color.White, 0, new Vector2(0.8f, 1.1f) * ballon.Bounds.Size.ToVector2(), SpriteEffects.None, 0);
            }

            if (texture != null)
            {
                spriteBatch.Draw(texture, new Rectangle((Position + offset).ToPoint(), (Size * scale).ToPoint()), null, this.owner.Color, 0, new Vector2(0.5f, 0.8f) * texture.Bounds.Size.ToVector2(), SpriteEffects.None, 0);
            }

            if (hat != null && drawHat)
            {
                spriteBatch.Draw(hat, new Rectangle((Position + offset).ToPoint(), (Size * (scale * 1.1f)).ToPoint()), null, Color.White, 0, new Vector2(0.45f, 0.85f) * texture.Bounds.Size.ToVector2(), SpriteEffects.None, 0);
            }


        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((Position + offset - (Size * new Vector2(0.5f, 0.8f))).ToPoint(), Size.ToPoint());
            }
        }

        public void Select()
        {
            this.isSelected = true;
            this.offset = offset = new Vector2(0, -0.5f) * Size;
            scale = new Vector2(1.5f);
        }

        public void UnSelect()
        {
            this.isSelected = false;
            this.offset = Vector2.Zero;
            scale = Vector2.One;
        }

        public void OnHover()
        {
            scale = new Vector2(1.5f);
        }

        public void OffHover()
        {
            if (!isSelected)
            {
                scale = Vector2.One;
            }
        }

        public bool IsActive
        {
            get
            {
                return this.isActive;
            }
        }

        public void Secure()
        {
            this.isSafe = true;
            this.Player.securePawns++;
        }

        public int DistanceToEnd
        {
            get
            {
                return distanceToEnd;
            }
            private set
            {
                distanceToEnd = value;
            }
        }

        public bool IsSafe
        {
            get
            {
                return this.isSafe;
            }
        }

        public int ID
        {
            get { return this.id; }
        }

        public override string ToString()
        {
            return $"pawn id {ID,2} player {Player.Nickname,-20} | is {(isActive ? "" : "not "),-4}active | is { (isSafe ? "" : "not "),-4}safe | is {(isSelected ? "" : "not "),-4}selected ";
        }
    }
}