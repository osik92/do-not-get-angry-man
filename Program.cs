﻿#define LOGGER_ENABLE

using System;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using GameEngine.Logger;
using GameEngine.Utilities;
using Microsoft.Xna.Framework;

namespace Do_not_get_angry_man
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        private static JsonLogger jsonLogger;
        private static string gameVersion = "642e472";

        //[STAThread]
        static void Main(string[] args)
        {
            jsonLogger = new JsonLogger();
            Logger.RegisterOutput(jsonLogger.Log);
            Logger.RegisterOutput(ConsoleLogger.Log);

            Logger.LogMessage(DeviceInformation.OSInformation());
            Logger.LogMessage(DeviceInformation.CPUInormation());
            Logger.LogMessage(DeviceInformation.GPUInormation());
            Logger.LogMessage("Username: " + Environment.UserName);
            Logger.LogMessage("Game version: " + gameVersion);

            try
            {
                using(var mainGame = new MainGame())
                {
                    mainGame.Run();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
            

            Logger.UnregisterOutput(ConsoleLogger.Log);
            Logger.UnregisterOutput(jsonLogger.Log);

            string filename = $"{(DateTime.Now.ToString("yyyyMMddHHmmss"))}.json";

            jsonLogger.SaveToFile(filename);

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = "SendLog.exe",
                Arguments = filename,
                WindowStyle = ProcessWindowStyle.Hidden
            };
            try
            {
                using(var exeProcess = Process.Start(startInfo))
                {
                    exeProcess?.WaitForExit();
                }
            }
            catch(Exception)
            {
                // ignored
            }
        }
    }
#endif
}
