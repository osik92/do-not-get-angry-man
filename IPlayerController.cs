﻿namespace Do_not_get_angry_man
{

    public delegate void OnPawnSelectHandler(Pawn pawn);
    public interface IPlayerController
    {
        bool IsRollOfDice();
        bool IsEndTurn();

        event OnPawnSelectHandler OnPawnSelect;

    }
}