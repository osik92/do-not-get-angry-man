﻿using System;
using System.IO;
using GameEngine;

namespace Do_not_get_angry_man
{
    public class Config
    {
        private Config()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), STORAGE_DIRECTORY);

            filename = Path.Combine(path, STORAGE_FILENAME);

            if (File.Exists(filename))
            {
                StorageReader sr = new StorageReader(filename);
                this.storage = sr.Load();
            }
            else
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                this.storage = new Storage();
            }
        }

        private static Config instance = null;
        private Storage storage;
        private const string STORAGE_FILENAME = "config.conf";
        private const string STORAGE_DIRECTORY = "Chinczyk";
        private string filename;
        public static Config Instance
        {
            get
            {
                if (instance == null)
                    instance = new Config();
                return instance;
            }
        }

        public Storage Storage
        {
            get { return this.storage; }
        }


        public void SaveStorage()
        {
            Storage.Save(filename);
        }
    }
}